# Notice

This is a fork of the [Official Opencast Matterhorn
Repository](https://bitbucket.org/opencast-community/matterhorn).


This fork is only used to send pull requests (to make sure certain people have
a graphical user interface for merging). If you are interested in my actual
working repository visit my 
[Opencast Matterhorn Repository on GitHub](https://github.com/lkiesow/opencast-matterhorn).
